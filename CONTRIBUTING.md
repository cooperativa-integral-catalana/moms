# How to contribute?

## Set up the dev env

```
# Clone the repo
$ git clone git@gitlab.com:cooperativa-integral-catalana/moms.git

# Configure the commit template
$ cd moms
$ git config commit.template docs/moms-commit-template

# Create a virtual env
$ pyvenv venv

# Activate the venv
$ source venv/bin/activate

# Install project dependencies
$ pip3 install -r requirements/development.pip

# Run migrations
$ cd moms
$ python3 manage.py migrate

# Run dev server
$ python3 manage.py runserver
```

## Run test suite

```
# Go to the testing directory
$ cd moms/testing

# Run code-check
$ tox -e code-check

# Run test suite
$ tox

```

## Run functional tests

```
# Download chrome driver
$ wget https://chromedriver.storage.googleapis.com/2.29/chromedriver_linux64.zip

# Extract it
$ unzip chromedriver_linux64.zip

# Copy chromedriver to your $PATH
$ sudo cp chromedriver /usr/bin/

# Go to moms testing dir
$ cd moms/testing

# Run functional test suite
$ tox -e functional
```

## Updating the test docs before pushing

Before submitting a MR make sure to have updated the test suite docs with the folowing set of commands:

```
# go to the scripts dir
$ cd scripts/

# update the test suite docs
$ ./update_test_docs.sh

# Commit the changes and push
```

If you prefer you could automatize this process with the [test suite docs pre-commit hook](scripts/pre-commit.README.md).
