#!/bin/bash

if ! git diff --quiet --cached -- moms/testing/unit_tests/ moms/testing/functional_tests/; then
    echo "THE UNIT-TEST SUITE HAS BEEN MODIFIED"

    echo "ACTIVATING VIRTUAL ENV IF NECESSARY"

    [ -z "$VIRTUAL_ENV" ] && source venv/bin/activate
    
    echo "UPDATING TEST SUITE DOCS..."

    sphinx-apidoc -f -M -o moms/testing/docs moms/testing

    sphinx-build -b singlehtml moms/testing/docs moms/testing/docs/html

    echo "DONE"

    echo "ADDING NEW DOCS TO YOUR COMMIT..."

    git add moms/testing/docs/

    echo "DONE"

fi

