# Test suite docs pre-commit hook 

## Motivation

This hook automatizes the test suite docs generation when there's a change in test directories. Once you installed it, it will check for any changes in test directories when you try to commit and, if any, it will:

* generate new docs
* add them to your current commit


## Installation

Run the following command from the main repo directory:

```
$ ln -s ${PWD}/scripts/pre-commit.sh .git/hooks/pre-commit
```


