from django.core.management.base import BaseCommand
from django.core.management.base import CommandError

from accounts.models import Manager
from accounts.models import MomsUser

from base64 import b64encode
from os import urandom


class Command(BaseCommand):
    help = "Create a manager user and outputs it password."

    def add_arguments(self, parser):
        parser.add_argument('username')

    def handle(self, *args, **options):
        try:
            user = MomsUser.objects.create(
                username=options['username'],
                email=options['username'],
            )
        except Exception as e:
            raise CommandError(
                "Ups! Username already exists or is not a valid email: %s"
                % e
            )
        pw = b64encode(urandom(64)).decode('utf-8')
        user.set_password(pw)
        user.save()

        Manager.objects.create(user=user)

        self.stdout.write(self.style.SUCCESS(
            "Congratulations! User created successfully..."
        ))
        self.stdout.write(self.style.SUCCESS(
            "Your new password is: %s" % pw
        ))
