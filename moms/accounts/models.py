from django.contrib.auth.models import User
from django.db import models


class MomsUser(User):
    """
    Proxy model to enhance :model:`auth.User` model logic.
    This will be our default User model.
    """
    class Meta:
        proxy = True

    def is_manager(self):
        """
        Designate whether the user has manager permissions.
        """
        return hasattr(self, 'manager')

    def is_member(self):
        """
        Designate whether the user has member permissions.
        """
        return hasattr(self, 'member')


class Manager(models.Model):
    """
    Profile model for manager users.
    """
    user = models.OneToOneField(MomsUser, on_delete=models.CASCADE)


class Member(models.Model):
    """
    Profile model for member users.
    """
    user = models.OneToOneField(MomsUser, on_delete=models.CASCADE)
