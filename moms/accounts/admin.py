from django.contrib import admin

from .models import MomsUser
from .models import Manager
from .models import Member


for model in [
        MomsUser,
        Manager,
        Member,
]:
    admin.site.register(model)
