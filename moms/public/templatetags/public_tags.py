from django import template


register = template.Library()


@register.filter()
def is_member(user):
    return hasattr(user, "member")
