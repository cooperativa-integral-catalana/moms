.. The Test Suite documentation master file, created by
   sphinx-quickstart on Sun Jun 11 17:05:49 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The Test Suite's documentation!
==========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   testing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
