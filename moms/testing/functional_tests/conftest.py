"""
Configuration file for pytest to describe new fixtures that will be used along
 the whole functional test session.
"""
import pytest

from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions
from splinter import Browser


@pytest.fixture(scope="session")
def browser():
    """This is a fixture-patch necessary for docker CI."""

    opt = ChromeOptions()
    opt.add_argument('--no-sandbox')

    browser = Browser('chrome', options=opt)
    return browser
