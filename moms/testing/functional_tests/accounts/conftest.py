"""
Configuration file for pytest to describe new fixture that will be used along
the ``accounts`` module.
"""
import pytest

from accounts.models import Manager
from accounts.models import Member
from accounts.models import MomsUser


@pytest.fixture
def user():
    """
    Fixture for a single MomsUser.
    """
    return MomsUser.objects.create_user(
        username="functional@test.org",
        email="functional@test.org",
        password="hello_tests",
    )


@pytest.fixture
def manager_user(user):
    """
    Fixture for a single manager User.
    """
    return Manager.objects.create(
        user=user
    ).user


@pytest.fixture
def member_user(user):
    """
    Fixture for a single member User.
    """
    return Member.objects.create(
        user=user
    ).user
