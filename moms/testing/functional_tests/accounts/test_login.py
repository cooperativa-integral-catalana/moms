"""
Module where the login-related tests are stored.
"""
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when
import pytest

from functools import partial


login_scenario = partial(scenario, "../features/accounts/login.feature")


@pytest.mark.django_db
@login_scenario("Manager user")
def test_manager_login(live_server, manager_user):
    pass


@pytest.mark.django_db
@login_scenario("Member user")
def test_member_login(live_server, member_user):
    pass


@given("I create a manager user")
def create_manager_user(manager_user):
    return manager_user


@given("I create a member user")
def create_member_user(member_user):
    return member_user


@given("I go to landing page")
def goto_landing_page(browser, live_server):
    browser.visit(live_server.url)


@given("I login as the manager user")
def login_manager_user(browser, manager_user):
    browser.find_by_id("id_username").fill(manager_user.username)
    browser.find_by_id("id_password").fill("hello_tests")


@given("I login as the member user")
def login_member_user(browser, member_user):
    browser.find_by_id("id_username").fill(member_user.username)
    browser.find_by_id("id_password").fill("hello_tests")


@given("I push the login button")
def push_login_button(browser):
    browser.find_by_id("login").click()


@then("I should be redirected to dashboard")
def redirect_to_dashboard(browser, live_server):
    assert browser.url == live_server.url + "/"
