Feature: Login
    From the landing page to the dashboard. A login process.

Scenario: Manager user
    Given I create a manager user  # Will use the database
    And I go to landing page
    And I login as the manager user
    And I push the login button
    Then I should be redirected to dashboard

Scenario: Member user
    Given I create a member user  # Will use the database
    And I go to landing page
    And I login as the member user
    And I push the login button
    Then I should be redirected to dashboard

    
    