"""
This is M.O.M.S. functional test suite.

All these tests are executed inside a browser with a browser driver.

The framework behind the suite is
`pytest <https://docs.pytest.org/en/latest/contents.html>`_ with the
`pytest-bdd <http://pytest-bdd.readthedocs.io/en/latest/>`_ plug-in and the
`pytest-splinter <http://pytest-splinter.readthedocs.io/en/latest/>`_ plug-in.

In the ``features/`` folder you can find the ``.feature`` files with
one or more **Scenarios**. Remember that **only** one Feature per
file is allowed.
"""
