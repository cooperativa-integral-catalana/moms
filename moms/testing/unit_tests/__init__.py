"""
This is M.O.M.S. unit test suite.

The framework behind the suite is
`pytest <https://docs.pytest.org/en/latest/contents.html>`_ with the
`pytest-django <http://pytest-django.readthedocs.io/en/latest/>`_ plug-in.
"""
