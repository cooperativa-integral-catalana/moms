import pytest

from accounts.models import Manager
from accounts.models import Member
from accounts.models import MomsUser


@pytest.fixture
def dumb_user():
    """
    Fixture for `MomsUser` instance.
    """
    user = MomsUser.objects.create(
        username="hello@example.org",
        email="hello@example.org",
    )
    user.set_password("qwertyuiop")
    user.save
    return user


@pytest.fixture
def dumb_manager(dumb_user):
    """
    Fixture for simple `Manager` instance.
    """
    manager = Manager.objects.create(
        user=dumb_user,
    )
    return manager


@pytest.fixture
def dumb_member(dumb_user):
    """
    Fixture for simple `Member` instance.
    """
    member = Member.objects.create(
        user=dumb_user,
    )
    return member


@pytest.fixture
def dumb_manager_and_member(dumb_user):
    """
    Fixture for both `Manager` and `Member`
    instances related with the same `MomsUser`.
    """
    manager = Manager.objects.create(
        user=dumb_user,
    )
    member = Member.objects.create(
        user=dumb_user,
    )
    return (manager, member)
