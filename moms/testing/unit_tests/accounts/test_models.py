"""
Module where unit tests related with ``accounts.models`` live.
"""
from django.conf import settings

import pytest


@pytest.mark.django_db
def test_user_is_only_manager(dumb_manager):
    """
    Test `MomsUser` methods when
    the user instance is only a manager.
    """
    assert dumb_manager.user.is_manager()
    assert not dumb_manager.user.is_member()


@pytest.mark.django_db
def test_user_is_only_member(dumb_member):
    """
    Test `MomsUser` methods when
    the user instance is only a member.
    """
    assert not dumb_member.user.is_manager()
    assert dumb_member.user.is_member()


@pytest.mark.django_db
def test_user_is_both_manager_and_member(dumb_manager_and_member):
    """
    Test `MomsUser` methods when
    the user instance is both member and manager.
    """
    for m in dumb_manager_and_member:
        assert m.user.is_manager()
        assert m.user.is_member()
