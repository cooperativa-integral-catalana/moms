"""
Module where unit tests related with ``accounts`` management commands live.
"""
from django.core.management import call_command
from django.core.management.base import CommandError
from django.utils.six import StringIO

from accounts.models import MomsUser

import pytest


@pytest.mark.django_db
def test_create_manager_user_command_when_username_already_exists(dumb_user):
    """
    Tests how fails the command when the username given already exists.
    """
    with pytest.raises(CommandError):
        existing_username = dumb_user.username
        out = StringIO()
        call_command("create_manager_user", existing_username, stdout=out)


@pytest.mark.django_db
def test_create_manager_user_command_integrity():
    """
    Test the actual user creation after calling the command.
    """
    new_username = "super@manager.net"
    call_command("create_manager_user", new_username)
    assert MomsUser.objects.filter(username=new_username).exists()
    new_user = MomsUser.objects.get(username=new_username)
    assert new_user.is_manager()


@pytest.mark.django_db
def test_create_manager_user_command_output():
    """
    Test the output command after calling the command.
    """
    new_username = "manager@moms.org"
    out = StringIO()
    call_command("create_manager_user", new_username, stdout=out)
    assert "Congratulations!" in out.getvalue()
    assert "Your new password is:" in out.getvalue()
