# Raw thoghts about MOMS' specs

## Intro

I would prefer to define roughly and sketchly the main features of this software and sharp them out with warm discussions in different issues rather than write down a formal document with a list of semi-infinite features detailed in every aspect and attribute.


## The three actors

More or less we could reduce the whole software into three possible interaction categories:

* the reader - who wants to consult the status of the acquired services.
* the red taper - who does formalities between the mobile career and the member
* the injector - who passively inserts data into the system useful for both reader and red taper.


### The reader

* It's basically a read-only user.
* It has access to the *n*-lines **only** related with its account.
* It has access to the price per month of the service.
* It has access to the *delivery notes* of its *n*-lines.
* It has access to the phone calls *metadata* of its *n*-lines.


### The red taper

* It's the *hero of the day*. It does all the job.
* It uploads the data that *the injector* provides.
* It manages the contracts.
* It manages the devices payments.
* It has access to **all** the accounting information.


### The injector

This actor is passive and enters in the play by the hands of *the red taper*.
It serves a `.txt` file that resemble from far a CSV file but it **isn't**.
In this file we have:

* Invoicing information
* Phone calls metadata
* Accounting information


## The two prices

Let's try not to forget that we are *sometimes* reselling the service the upstream career is giving to us. So we should have in consideration a double accounting. The first, sided towards the provider, and the second, sided towards the member.


## Dashboards

It would be great to take benefit of the front-end frameworks we have choosen to have beautiful (or at least not awful) visualizations of the info condensed for our users.


## Next steps

* Think out a label system.
* Discuss features with issues.

