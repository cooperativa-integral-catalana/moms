# Zero-Day meeting for MOMS

## Agenda

### overview del repo en general


### metodología de documentacion

TODO: buscar framework de documentación


### workflow de git

* master eterna
* ramas de desarrollo desechables
* MR fast-forward


CUESTIONES DE GIT

a) Voy a empezar a desarrollar una nueva funcionalidad/bug/test

1) Nos movemos hacia master [git checkout master]
2) Actualizamos nuestra copia local [git pull]
3) Creamos una nueva rama y nos movemos a ella [git checkout -b <nombreRama>]
4) Hack....hack....hack... (y commits)
5) Hacemos un git status y:
       - Estamos en la rama local propia de desarrollo <nombreRama>
       - No tenemos nada pendiente (commits o add)
6) git push origin <nombreRama>
7) Desde la interfaz de gitlab empezamos una MR
8) Cuando se acepte, git checkout master
9)  git pull
10) Ya podemos tirar <nombreRama> con git branch -d <nombreRama>

b) Voy a continuar con mi desarrllo de la rama, pero no se si hay cambios en remoto

1) Hacemos un git status y:
       - Estamos en la rama local propia de desarrollo <nombreRama>
       - No tenemos nada pendiente (commits o add)
2) Nos movemos hacia master [git checkout master]
3) Acualizamos nuestra copia local de master [git pull]
4) Si hay cambios:

         4.1) Nos movemos a nuestra rama propia [git checkout <nombreRama>
         4.2) Hacemos un rebase  contra la rama master [git rebase master]
         4.3) arreglamos conflictos :)

5) Si no hay cambios:
        5.1) Llama a tus compañeros para que curren un poco más.

TODO:  pass this notes to docs/meeting-notes-0.md

TODO: añadir docu sobre como ejecutar tests en local

TODO: añadir docu sobre test driven development with continuous integration


### como queremos usar los tickets (ejemplo ticket #1)

### debatir las especificaciones pobres

### pasar las especificaciones a debates en tickets

### Antepasados del proyecto y contextualización en el mundo
