# Commit template

## How to use

Run:

```
$ git config commit.template docs/moms-commit-template`
```


## Commit Template Format Explained

```
[TYPE] <subject>

<body>
<footer>
```

Type should be one of the following:
- bug (bug fix)
- feat (new feature)
- docs (changes to documentation)
- style (formatting, pep8 violations, etc; no code change)
- refactor (refactoring production code)
- test (adding missing tests, refactoring tests; no production code change)
- i18n translation related changes

Subject should use imperative tone and say what you did.
For example, use 'change', NOT 'changed' or 'changes'.

The body should go into detail about changes made.

The footer should contain any issue references or actions.
You can use one or several of the following:

- Fixes: #XYZ
- Related: #XYZ
- Documentation: #XYZ


The Documentation field should be included in every new feature commit, and it
should link to an issue in the bug tracker where the new feature is analyzed
and documented.

For a full example of how to write a good commit message, check out
https://chris.beams.io/posts/git-commit/

