[![build status](https://gitlab.com/cooperativa-integral-catalana/moms/badges/master/build.svg)](https://gitlab.com/cooperativa-integral-catalana/moms/commits/master) [![coverage report](https://gitlab.com/cooperativa-integral-catalana/moms/badges/master/coverage.svg)](https://gitlab.com/cooperativa-integral-catalana/moms/commits/master)

# Mobile Oriented Management System

Simple software for managing CICMob project based on django and bootstrap.


## System wide dependencies (Debian Stretch)

* python3
* python3-dev
* python3-venv
* python3-pip


## Functional test suite system-wide dependencies (Debian Stretch)

* chromedriver (see instructions on [CONTRIBUTING.md](CONTRIBUTING.md))
* libgconf-2-4
* chromium
* default-jre
* xvfb

